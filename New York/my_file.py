import pandas as pd
import datetime

import sys

def change_dataframe(dataframe):
    columns = ['Date','Count']
    df = pd.DataFrame(columns=columns)
    for i in range(len(dataframe.index)) :
        hour = 0
        for x in range(24):
            date = dataframe.iloc[i,6] + datetime.timedelta(hours=hour)
            if hour == 0:
                #count = dataframe['12:00-1:00 AM'][i]
                count = dataframe.iloc[i,7]
            elif hour ==1 :
                #count = dataframe['1:00-2:00AM'][i]
                count = dataframe.iloc[i,8]
            elif hour ==2 :
                #count = dataframe['2:00-3:00AM'][i]
                count = dataframe.iloc[i,9]
            elif hour ==3 :
                #count = dataframe['3:00-4:00AM'][i]
                count = dataframe.iloc[i,10]
            elif hour ==4 :
                #count = dataframe['4:00-5:00AM'][i]
                count = dataframe.iloc[i,11]
            elif hour ==5 :
                #count = dataframe['5:00-6:00AM'][i]
                count = dataframe.iloc[i,12]
            elif hour ==6 :
                #count = dataframe['6:00-7:00AM'][i]
                count = dataframe.iloc[i,13]
            elif hour ==7 :
                #count = dataframe['7:00-8:00AM'][i]
                count = dataframe.iloc[i,14]
            elif hour ==8 :
                #count = dataframe['8:00-9:00AM'][i]
                count = dataframe.iloc[i,15]
            elif hour ==9 :
                #count = dataframe['9:00-10:00AM'][i]
                count = dataframe.iloc[i,16]
            elif hour ==10 :
                #count = dataframe['10:00-11:00AM'][i]
                count = dataframe.iloc[i,17]
            elif hour ==11 :
                #count = dataframe['11:00-12:00PM'][i]
                count = dataframe.iloc[i,18]
            elif hour ==12 :
                #count = dataframe['12:00-1:00PM'][i]
                count = dataframe.iloc[i,19]
            elif hour ==13 :
                #count = dataframe['1:00-2:00PM'][i]
                count = dataframe.iloc[i,20]
            elif hour ==14 :
                #count = dataframe['2:00-3:00PM'][i]
                count = dataframe.iloc[i,21]
            elif hour ==15 :
                #count = dataframe['3:00-4:00PM'][i]
                count = dataframe.iloc[i,22]
            elif hour ==16 :
                #count = dataframe['4:00-5:00PM'][i]
                count = dataframe.iloc[i,23]
            elif hour ==17 :
                #count = dataframe['5:00-6:00PM'][i]
                count = dataframe.iloc[i,24]
            elif hour ==18 :
                #count = dataframe['6:00-7:00PM'][i]
                count = dataframe.iloc[i,25]
            elif hour ==19 :
                #count = dataframe['7:00-8:00PM'][i]
                count = dataframe.iloc[i,26]
            elif hour ==20 :
                #count = dataframe['8:00-9:00PM'][i]
                count = dataframe.iloc[i,27]
            elif hour ==21 :
                #count = dataframe['9:00-10:00PM'][i]
                count = dataframe.iloc[i,28]
            elif hour ==22 :
                #count = dataframe['10:00-11:00PM'][i]
                count = dataframe.iloc[i,29]
            elif hour ==23 :
                #count = dataframe['11:00-12:00AM'][i]
                count = dataframe.iloc[i,30]
            #hour = date.hours
            #dayoftheweek = date.dayoftheweek
            new_row={'Date':date,'Count':count}
            df = df.append(new_row, ignore_index=True)
            hour+=1
    return df

#dataset = pd.read_csv("Traffic_Volume_Counts__2014-2019_.csv")
#dataset["Date"] = pd.to_datetime(dataset["Date"],format='%m/%d/%Y')
#third_avenue = dataset[dataset["Roadway Name"]=="3 Avenue"]
#third_avenue_NB = third_avenue[third_avenue["Direction"]=="NB"]
#third_avenue_test = third_avenue_NB[third_avenue_NB["Segment ID"]==70376]
#print(change_dataframe(third_avenue_test))
